# TIC TAC TOE
---------------

Um simples jogo da velha onde contem 3 tipos de modalidades na quais estão:

Computador x Computador | Player x Player | Player x Computador 

e Na opção de 3 na qual é Computador x Computador pode ser escolhido 3 tipos de niveis:

Fácil | Médio | Difícil  


## Instalação do Projeto
------------------------

### Dependências

 - Ruby v2.4.0
     - Caso não tenha Ruby intalado em sua máquina, recomendamos o uso do [RVM](https://rvm.io/) para a instalação do mesmo.
 - Git e Git Flow
 
**Download**

Acesse via terminal o local dos seus projetos e faça o download do repositório.
```bash
$ git clone git@bitbucket.org:AugustoSandim/forum-web.git
```

Para utilização do ambiente de desenvolvimento configure o Git Flow.
```bash
$ sudo apt-get install git-flow
$ git flow init
```
Pressione Enter para todas as opções que serão exibidas. Depois disso você já estara na branch de development.

Caso opte por não utilizar Git Flow, é necessário fazer o download da branch `develop` remota.

**Configuração do Gemset**

Dentro da pasta do projeto, crie os arquivos `.ruby-version` e `.ruby-gemset` e depois entre novamente na pasta para carregar as novas configurações.
```bash
$ echo "ruby-2.4.0" > .ruby-version
$ echo "forum" > .ruby-gemset
$ cd .
```

**Rodar o jogo**

Acesse via terminal o local onde fez o clone e acesse a pasta do jogo
```bash
$ ruby main.rb
```

e divirta-se
