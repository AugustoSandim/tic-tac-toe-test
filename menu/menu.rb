class Menu

  def menu_players(game)
    spot = nil
    selected = "S E L E C I O N A D O".green

    puts "Selecione a modalidade do jogo:\n\n"
    puts "1 - Computador x Computador"
    puts "2 - Player x Player"
    puts "3 - Player x Computador"
    puts "0 - Sair"

    until spot
      spot = gets.chomp.to_i

      if spot == 1
        puts "\nComputador x Computador #{selected}"

        game.computer_to_computer
      elsif spot == 2
        puts "\nPlayer x Player #{selected}"

        game.player_to_player
      elsif spot == 3
        puts "\nPlayer x Computador #{selected}"

        level = Level.new
        level.levels(game)
      elsif spot == 0
        puts "Até logo :)"

        spot = nil

        return
      else
        puts "Entrada inválida. Use o numérico para selecionar corretamente e aperte tecla enter"

        spot = nil

        menu_players(game)
      end
    end
  end

end

